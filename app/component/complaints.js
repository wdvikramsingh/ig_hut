import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);

import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView,
    Alert,
    Modal,
   
} from 'react-native';

export default function Complaints({ navigation }) {

    const [modalVisible, setModalVisible] = React.useState(false);
    const [passwordShown, setPasswordShown] = React.useState(false);

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    return (

        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
            <View style={styles.right_slide}>
                    <TouchableOpacity onPress={() => { setModalVisible(!modalVisible); }}>
                        <Text style={styles.close}> X Close</Text>
                    </TouchableOpacity>
                    <Text style={styles.history}>Complaint History</Text>
                </View>
                </Modal>

                 

            <View style={styles.leads}>
                <SafeAreaView>
                    <ScrollView>
               
                <View style={styles.head}>
                    <TouchableOpacity style={styles.menu}>
                        <Image source={require('../assets/images/back.png')} style={styles.bar} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.sart}>
                        <Text style={styles.sarthitext}> Complaints</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.home} onPress={() => {
                        setModalVisible(true);
                    }}
                    >
                        <Image source={require('../assets/images/menu.png')} style={styles.home} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.inspection} onPress={(props) => navigation.navigate('Electrician', {complnt: 'Electrician'})}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/electrician.png')} />
                    <Text style={styles.welcome}>Electrician</Text>
                   
                </TouchableOpacity>

                <TouchableOpacity style={styles.inspection} onPress={(props) => navigation.navigate('Plumber')}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/plumber.png')} />
                    <Text style={styles.welcome}>Plumber</Text>

                </TouchableOpacity>


                <TouchableOpacity style={styles.inspection} onPress={(props) => navigation.navigate('Carpenter')}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/carpenter.png')} />
                    <Text style={styles.welcome}>Carpenter</Text>

                        </TouchableOpacity>
                        <TouchableOpacity style={styles.inspection} onPress={togglePasswordVisiblity}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/clipboard.png')} />
                    <Text style={styles.welcome}>Other</Text>
                        </TouchableOpacity>
                        <View style={styles.other_section}>
                            
                            {
                                passwordShown ? <TextInput style={styles.other_sys} placeholder="Others" /> : null
                            }
                        </View>
                    </ScrollView>
                </SafeAreaView>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                        </View>
                       
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },
    welcome: {

        fontSize: 20,
        textAlign: "center",
        paddingTop: 10,
        paddingBottom:10,


    },

    inspection: {

        width: 370,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        marginTop: 20,
        overflow: 'hidden',

    },
    
    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000',
        overflow: 'hidden'



    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    complaints: {

        textAlign: 'center',
        fontSize: 20,
        color: 'white'
        

    },

    complaint_holder: {

        backgroundColor: 'red',
        height: 50,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
        
    },


    tinyLogo: {

        width: 70,
        height: 70,
        marginTop:10,

    },

    head: {


        flexDirection: 'row',
        backgroundColor: 'red',
        height: 50,
        justifyContent: "space-between",
        alignItems: 'center'

    },

    bar: {

        width: 40,
        height: 25,
        marginLeft: 10,
        

    },

    sarthitext: {

        fontSize: 20,
        color: '#fff',
        
    },

    home: {
        width: 40,
        height: 30,
        marginRight: 10,
    },

    right_slide: {

        position: "absolute",
        right: 0,
        top: 0,
        bottom: 0,
        width: 250,
        backgroundColor: 'white',
        zIndex:11111,

    },

    history: {

        textAlign: 'center',
        paddingTop: 20,
        fontSize:20,
    },

    close: {

        fontSize: 20,
        paddingLeft: 20,
        paddingTop: 10,
        height:50,
        backgroundColor: '#730204',
        color:'white'
    },

    other_section: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 80,   
    },

    other_sys: {
        width: 370,
        height: 40,
        borderColor: 'grey',
        borderWidth: 2,
        borderRadius: 5,
        fontSize: 18,
        paddingLeft: 10,
        
        
    },
});