import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);

import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView
} from 'react-native';

export default function Feedbackrating({ navigation }) {
    return (

        <>
            <View style={styles.leads}>
                <View style={styles.inspection}>
                    <Text style={styles.welcome}> Case: Plumber required (#12345) </Text>
                   


                </View>

                <View style={styles.inspection}>
                    <Text style={styles.welcome}>Your feedback is very important for us to ensure the good health of our services </Text>
                </View>
                <View style={styles.star_holder}>
                    <Image style={styles.star} source={require('../assets/images/star.png')} />
                    <Image style={styles.star} source={require('../assets/images/star.png')} />
                    <Image style={styles.star} source={require('../assets/images/star.png')} />
                    <Image style={styles.star} source={require('../assets/images/star.png')} />
                    <Image style={styles.star} source={require('../assets/images/star.png')} />
                </View>
                <TouchableOpacity>
                    <Text style={styles.rate}>Rate 4</Text>
                    <Text style={styles.rate}>Rate 3</Text>
                    <Text style={styles.rate}>Rate 2</Text>

                </TouchableOpacity>
               

                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },
    welcome: {

        fontSize: 20,
        textAlign: "center",
        padding: 20,


    },

    inspection: {

        width: 370,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        marginTop: 50,
        overflow: 'hidden',

    },
    pic_men: {
        justifyContent: "space-evenly",
        flexDirection: "row",
        marginTop: 10,
        width: screenWidth,
        backgroundColor: 'red',

    },

    text_pics: {
        textAlign: 'center',
        color: '#fff',
        padding: 8,

    },

    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000'



    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },
    rate: {

        backgroundColor: 'red',
        color: 'white',
        borderRadius: 5,
        height: 50,
        width: 370,
        textAlign: 'center',
        alignSelf: 'center',
        marginTop: 20,
        paddingTop: 13,
        fontSize:17,


    },

    star_holder: {

        flexDirection: 'row',
        alignSelf: 'center',
        marginTop:20,


    },
    star: {

        width: 40,
        height: 40,
        margin:7,
        


    }

});