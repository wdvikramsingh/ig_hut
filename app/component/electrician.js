import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);

import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView,
    
} from 'react-native';

export default function Electrician({ navigation, route }) {

    const { complnt } = route.params;

    const [passwordShown, setPasswordShown] = React.useState(false);

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    return (
        <>
            <View style={styles.leads}>
                <View style={styles.head}>
                    <TouchableOpacity style={styles.menu} onPress={(props) => navigation.navigate('Complaints')}>
                        <Image source={require('../assets/images/back.png')} style={styles.bar} />
                    </TouchableOpacity>
                    <View style={styles.sart}>
                        <Text style={styles.sarthitext}> Electrician</Text>
                    </View>
                    <View style={styles.home}>
                        
                    </View>
                </View>
                <TouchableOpacity style={styles.inspection} onPress={(props) => navigation.navigate('Issuewithac', { complaints: complnt, acsystem:'AC' })}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/air-conditioner.png')} />
                    <Text style={styles.welcome}>AC</Text>

                </TouchableOpacity>

                <TouchableOpacity style={styles.inspection}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/washing-machine.png')} />
                    <Text style={styles.welcome}>Washing Machine</Text>

                </TouchableOpacity>


                <TouchableOpacity style={styles.inspection}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/kitchen.png')} />
                    <Text style={styles.welcome}>Fridge</Text>

                </TouchableOpacity>
                <TouchableOpacity style={styles.inspection} onPress={togglePasswordVisiblity}>
                    <Image style={styles.tinyLogo} source={require('../assets/images/clipboard.png')} />
                    <Text style={styles.welcome}>Other</Text>
                </TouchableOpacity>
                <View style={styles.other_section}>

                    {
                        passwordShown ? <TextInput style={styles.other_sys} placeholder="Others" /> : null
                    }
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },
    welcome: {

        fontSize: 20,
        textAlign: "center",
        paddingTop: 10,
        paddingBottom: 10,


    },

    inspection: {

        width: 370,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        marginTop: 20,
        overflow: 'hidden',

    },
    pic_men: {
        justifyContent: "space-evenly",
        flexDirection: "row",
        marginTop: 10,
        width: screenWidth,
        backgroundColor: 'red',

    },


    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000',
        overflow: 'hidden'



    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    complaints: {

        textAlign: 'center',
        fontSize: 20,
        color: 'white'


    },

    complaint_holder: {

        backgroundColor: 'red',
        height: 50,
        justifyContent: 'center'

    },


    tinyLogo: {

        width: 70,
        height: 70,
        marginTop: 10,

    },

    head: {


        flexDirection: 'row',
        backgroundColor: 'red',
        height: 50,
        alignItems: 'center'

    },

    bar: {

        width: 40,
        height: 25,
        marginLeft: 10,

    },

    sarthitext: {

        fontSize: 20,
        color: '#fff',
        paddingLeft:20,
        

    },

    home: {
        width: 40,
        height: 30,
        marginRight: 10,
    },

    other_section: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 80,
    },

    other_sys: {
        width: 370,
        height: 40,
        borderColor: 'grey',
        borderWidth: 2,
        borderRadius: 5,
        fontSize: 18,
        paddingLeft: 10,


    },
});