import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);
import {
    StyleSheet,
    Text,
    View,
    Alert,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView,
    
} from 'react-native';
export default function Otp({ navigation, route }) {

    const [otp, setOtp] = React.useState(null)
    

    const { mobileno } = route.params;



    const validationOtp = async () => {

       

        const url = 'http://it.igcoco.com/api/sms-otp-login?mobile=' + mobileno + '&engagement_id=9&otp=' + otp

        console.log(url)

        fetch(url, { method: 'GET' }).then(res => res.json())
            .then(response => {
                if (response.responseCode == '200') {

                    navigation.navigate('Start')

                } else {

                    alert(response.data.messageDescription)
                }
                console.log(response)
                console.log(url)
                

            }).catch(error => alert('Login Failed, Please Login Again'));
    }


    const otpValidation = () =>
        Alert.alert(
            "Mobile Number",
            "Please enter Otp",
            [

                { text: "OK" }
            ],
            { cancelable: false }
        );


    return (
        <>
            <View style={styles.leads}>
                <View style={styles.dodtsholder}>
                    <View style={styles.dodts}></View>
                    <View style={styles.dodts2}></View>
                    
                </View>
                <View>
                    <Image source={require('../assets/images/icon.png')} style={styles.leaddown} />
                </View>
                <View style={styles.verifcode}>
                    <Text style={styles.verifation}>Verification </Text>
                    <Text style={styles.code}>Enter OTP code sent to your number </Text>
                </View>
                <View style={styles.registration}>
                    <View style={styles.regifrom}>
                       
                       
                        <TextInput
                            label='otp'
                            keyboardType='number-pad'
                            style={styles.input_email}
                            onChangeText={(e) => { setOtp(e) }}
                            value={otp}
                        />
                    </View>
                    <View>
                        <TouchableOpacity onPress={(props) => {

                            if (otp == '' || otp == null) {

                                { otpValidation() }

                            } else if (otp.length != '6') {

                                alert('please enter valid otp')
                            } else {

                                { validationOtp() }

                            }

                        }}><Text style={styles.submit}>Submit  </Text></TouchableOpacity>
                        
                        <Text style={styles.resend} onPress={() => navigation.navigate('Login')}>Resend code </Text>
                    </View>
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
    },
    leaddown: {
        alignSelf: 'center',
        width: 120,
        height: 140,
        marginTop: 30,
    },
    registration: {
        width: 320,
        height: 340,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    input_email: {
        backgroundColor: '#f5f5f5',
        fontSize: 20,
        borderRadius: 15,
        marginTop: 12,
        paddingLeft: 25,
        width: 250,
        height: 60,
    },
    submit: {
        backgroundColor: '#de1d23',
        color: '#fff',
        width: 170,
        height: 55,
        fontSize: 22,
        paddingLeft: 10,
        paddingTop: 10,
        borderRadius: 25,
        textAlign: 'center',
        alignSelf: 'center',
    },
    verifation: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    code: {
        textAlign: 'center',
    },
    verifcode: {
        marginTop: 30,
        marginBottom: 20,
    },
    dodts: {
        width: '50%',
        height: 15,
        backgroundColor: '#f5f5f5',
        alignSelf: 'center',
        marginLeft: 10,
        borderRadius: 40,
    },
    dodts2: {
        width: '50%',
        height: 15,
        backgroundColor: '#de1d23',
        alignSelf: 'center',
        marginLeft: 10,
        borderRadius: 40,
    },
    dodtsholder: {
        justifyContent: "space-evenly",
        flexDirection: "row",
        marginTop: 30,
    },
    regifrom: {
        flex: 0.7,
        marginTop: 50,
        justifyContent: "space-evenly",
        flexDirection: "row",
    },
    received: {
        textAlign: 'center',
        paddingTop: 15,
    },
    resend: {
        textAlign: 'center',
        paddingTop: 10,
        color: 'blue'
    },
});