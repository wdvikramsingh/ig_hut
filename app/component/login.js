import React, { Component } from 'react'; 
const screenWidth = Math.round(Dimensions.get('window').width);
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView,
    Alert,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard  
} from 'react-native';
export default function Login( {navigation} ) {
    const [phone, setPhone] = React.useState(null)



    let current_time = new Date()

    let formatted_time = current_time.getHours() + ":" + current_time.getMinutes()
    console.log(formatted_time)

    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-0" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate()
    console.log(formatted_date)
    
    const loginOtp = () => {

        const url = 'http://it.igcoco.com/api/generate-otp-tm?mobile=' + phone + '&engagement_id=9&ig_date=' + formatted_date + '&ig_time=' + formatted_time

        console.log(url)

        fetch(url, { method: 'GET' }).then(res => res.json())

            .then(response => {

                if (response.responseCode == '200') {

                    navigation.navigate('Otp', { mobileno: phone})

                } else {
                    alert(response.data.messageDescription)
                }

                console.log(response)
                
                
                
                

        }).catch(error => alert('Login Failed, Please Login Again'));
    }



    const phoneValidation = () => {

        Alert.alert(
            "Mobile Number",
            "Please enter Mobile Number",
            [

                { text: "OK" }
            ],
            { cancelable: false }
        );
    }
        

        return (
            <>
                
                <View style={styles.leads}>
                <ScrollView>
                    <View style={styles.dodtsholder}>
                        <View style={styles.dodts}></View>
                        <View style={styles.dodts2}></View>
                       
                    </View>
     
                    <View>
                            <Image source={require('../assets/images/icon.png')} style={styles.leaddown} />
                    </View>
                    <View style={styles.verifcode}>
                        <Text style={styles.numberm}>Enter Your Mobile Number we will send you OTP to verify </Text>                    
                    </View>
                   
                    <View style={styles.registration}>
             
                        <View style={styles.regifrom}>                    
                            <TextInput
                                label='Name'
                                keyboardType='number-pad'
                                placeholder='Mobile Number'
                                style={styles.input_email}
                                onChangeText={(e) => { setPhone(e)}}
                                value={phone}
                                />    
                                 
                        </View>
                            <View style={styles.otp_padding}>
                                <TouchableOpacity onPress={(props) => {

                                    if (phone == '' || phone == null) {

                                        { phoneValidation() }

                                    } else if (phone.length != '10') {

                                        alert('please enter valid number')
                                    }else {

                                    { loginOtp() }
                                   
                                    
                            }

                                }}><Text style={styles.submit}>Get OTP</Text></TouchableOpacity>                      
                        </View>
               
                    </View>
                    </ScrollView>        
                </View>
                
            </>
        );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        padding:20,
        paddingTop:0,
    },
    leaddown: {
        alignSelf: 'center',
        width: 120,
        height: 140,
        marginTop: 30,
    },
    registration: {
        width: screenWidth - 60,
       // height: 240,
        backgroundColor: '#fff',
        alignSelf: 'center',
        borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 5.84,
        elevation: 5,
        marginBottom:20,
    },
    otp_padding: {
        paddingTop: 25,
        paddingBottom: 25,
    },
    input_email: {
        backgroundColor: '#f5f5f5',
        fontSize: 20,
        borderRadius: 15,
        
        paddingLeft: 25,
        width: screenWidth - 80,
        height: 60,
    },

    numberm: {

        fontSize: 18,
        paddingBottom: 10,
        paddingLeft: 10,
        marginTop: 12,
        textAlign: 'center',
        

    },
    submit: {
        backgroundColor: '#de1d23',
        color: '#fff',
        width: 170,
        height: 55,
        fontSize: 22,
        paddingTop: 12,
        borderRadius: 25,
        textAlign: 'center',
        alignSelf: 'center',
        
    },
    verifation: {
        textAlign: 'center',
       // fontWeight: 'bold',
        fontSize: 25,
        
    },
    code: {
        textAlign: 'center',
    },
    verifcode: {
        marginTop: 30,
        marginBottom: 20,
    },
    dodts: {
        width: '50%',
        height: 15,
        backgroundColor: '#de1d23',
        alignSelf: 'center',
        marginLeft: 10,
        borderRadius: 40,
    },
    dodts2: {
        width: '50%',
        height: 15,
        backgroundColor: '#f5f5f5',
        alignSelf: 'center',
        marginLeft: 10,
        borderRadius: 40,
    },
    

    dodtsholder: {
        justifyContent: "space-evenly",
        flexDirection: "row",
        marginTop: 30,
    },
    regifrom: {
        flex: 1,
        marginTop: 20,
        alignSelf: 'center',
               
    },
   
});