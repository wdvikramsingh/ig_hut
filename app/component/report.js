import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);

import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView
} from 'react-native';

export default function Report({ navigation, route}) {

    const { com, acys, cool, noice } = route.params;
    return (
        <>
            <View style={styles.leads}>
                <View style={styles.head}>
                    <TouchableOpacity style={styles.menu} onPress={(props) => navigation.navigate('Issuewithac')}>
                        <Image source={require('../assets/images/back.png')} style={styles.bar} />
                    </TouchableOpacity>
                    <View style={styles.sart}>
                        <Text style={styles.sarthitext}>Report</Text>
                    </View>
                    <View style={styles.home}>

                    </View>
                </View>

                <View>
                    <Text style={styles.report}>You are looking for {com} , for your product {acys} with the issue {cool} {noice}. </Text>


                    </View>


                <TouchableOpacity style={styles.submit}>
                    <Text style={styles.submit_holder}> Submit
                        </Text>
                    </TouchableOpacity>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },

    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000',
        overflow: 'hidden'



    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },


    head: {


        flexDirection: 'row',
        backgroundColor: 'red',
        height: 50,

        alignItems: 'center'

    },

    bar: {

        width: 40,
        height: 25,
        marginLeft: 10,


    },

    sarthitext: {

        fontSize: 20,
        color: '#fff',
        paddingLeft: 20,

    },

    home: {
        width: 40,
        height: 30,
        marginRight: 10,
    },

    report: {

        fontSize: 18,
        padding: 20,
        textAlign: 'justify'

    },
    submit: {

        backgroundColor: 'red',
        width: 150,
        height: 40,
        borderRadius: 4,
        alignSelf: 'center',
        justifyContent: 'center',
        
    },

    submit_holder: {

        color: 'white',
        textAlign: 'center',
        fontSize:18,



    },

});