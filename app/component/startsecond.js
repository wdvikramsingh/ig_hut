import React, { Component, useState } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);
import ImagePicker from 'react-native-image-picker';


import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView,
    CheckBox,
    Modal,
    Alert,
    TouchableHighlight
} from 'react-native';

export default function Startsecond({ navigation }) {

    const [avatarSource, setavatarSource] = React.useState(null)
    const [avatarSourcesecond, setavatarSourcesecond] = React.useState(null)
    const [avatarSourcethree, setavatarSourcethree] = React.useState(null)
    const [pic, setpic] = React.useState(null)
    const [isSelected, setSelection] = React.useState(false);
    const [selected, setSelected] = React.useState(false);
    const [isPress, setIsPress] = React.useState(false);
    const [modalVisible, setModalVisible] = React.useState(false);

    const options = {
        title: 'Select File',
        takePhotoButtonTitle: 'Take photo',
        chooseFromLibraryButtonTitle: 'Choose photo from library',
    }



    const touchProps = {
        activeOpacity: 1,
        underlayColor: 'green',
        style: isPress ? styles.btnPress : styles.btnNormal,
        onHideUnderlay: () => setIsPress(false),
        onShowUnderlay: () => setIsPress(true),
        onPress: () => console.log('HELLO'),
    };

    const myfun = () => {

        ImagePicker.showImagePicker(options, (response) => {
            /*console.log('Response = ', response);*/

            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('Image Picker Error: ', response.error);
            }

            else {
                let source = { uri: response.uri };
                let sourcesecond = { uri: response.uri };
                let sourcethree = { uri: response.uri };



                setavatarSourcesecond(source)
                setavatarSourcethree(sourcesecond)
                setavatarSource(sourcethree)
                setpic(response.data)



            }
        });
    }


    return (

        <>



            <View style={styles.leads}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>

                            <Image source={avatarSource}
                                style={{ width: screenWidth, marginTop: 100, marginBottom: 80, height: 300, borderWidth: 2, borderColor: '#000', padding: 20, }} />

                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                                onPress={() => {
                                    setModalVisible(!modalVisible);
                                }}
                            >
                                <Text style={styles.textStyle}>Hide</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
                <View style={styles.inspection}>
                    <Text style={styles.welcome}>Is there any  Termite in the House?</Text>

                </View>

                <View style={styles.capture}>
                    <TouchableHighlight onPress={() => { setModalVisible(true); }}>
                        <Image source={avatarSource}
                            style={{ width: 50, margin: 10, height: 50, borderWidth: 2, borderColor: '#000', padding: 20, }} />
                    </TouchableHighlight>

                </View>
                <View>
                    <TouchableHighlight onPress={touchProps, myfun} style={styles.pic_men}><Text style={styles.text_pics}>Yes</Text></TouchableHighlight >
                </View>
                <View>
                    <TouchableHighlight {...touchProps} style={styles.pic_men}><Text style={styles.text_pics}>No</Text></TouchableHighlight >

                </View>

                <View style={styles.pic_men}>
                    <TouchableOpacity {...touchProps} style={styles.button_holder} onPress={() => navigation.navigate('Startthird')}><Text style={styles.text_pics}>Next</Text></TouchableOpacity>

                </View>

                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                </View>
            </View>



        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },
    welcome: {

        fontSize: 20,
        textAlign: "center",
        padding: 40,


    },

    inspection: {

        width: 370,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 100,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        overflow: 'hidden',

    },
    pic_men: {
        alignSelf: 'center',
        marginTop: 10,
        width: 370,
        height: 50,
        backgroundColor: 'red',
        borderRadius: 4,


    },

    text_pics: {
        textAlign: 'center',
        color: '#fff',
        paddingTop: 15,

    },


    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000'

    },

    capture: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        width: 200,
        alignSelf: 'center',
    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },
    checkbox: {
        color: '#fff',
        backgroundColor: '#fff',

        marginTop: 10,
        marginRight: 10,
    },

    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
        justifyContent: 'center'
    },

    btnNormal: {
        alignSelf: 'center',
        marginTop: 10,
        width: 370,
        height: 50,
        backgroundColor: 'red',
        borderRadius: 4,
    },
    btnPress: {
        alignSelf: 'center',
        marginTop: 10,
        width: 370,
        height: 50,
        backgroundColor: 'green',
        borderRadius: 4,
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",

    },
    modalView: {

        backgroundColor: "black",

        flex: 1,
        alignItems: "center",

    },
    openButton: {
        backgroundColor: "#F194FF",

        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        width: 80,
        height: 20,


    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }

});