import React, { Component } from 'react';
const screenWidth = Math.round(Dimensions.get('window').width);

import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    TouchableOpacity,
    Input,
    TextInput,
    Button,
    SafeAreaView,
    ScrollView
} from 'react-native';

export default function Feedbackcomplaints({ navigation }) {
    return (

        <>
            <View style={styles.leads}>
                <View style={styles.inspection}>
                    <Text style={styles.welcome}> Plumber required (#12345) Executed on 20th Apr 20 </Text>
                    <View style={styles.pic_men}>
                        <TouchableOpacity style={styles.button_holder}><Text style={styles.text_pics}>status: Open</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.button_holder}><Text style={styles.text_pics}>Cost: 200</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.button_holder}><Text style={styles.text_pics}>Feedback</Text></TouchableOpacity>

                    </View>


                </View>

                <View style={styles.inspection}>
                    <Text style={styles.welcome}>Ekectricia requird ( #1234) Executed on 20th Jan 20 </Text>
                   

                </View>

                <View style={styles.footer}>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text}> Home</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text2}> Inpection</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text3}> Complaints</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.footer_touch}><Text style={styles.footer_text4}> Refer</Text></TouchableOpacity>
                </View>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    leads: {
        backgroundColor: '#fff',
        color: '#000',
        flex: 1,
        fontSize: 35,

    },
    welcome: {

        fontSize: 20,
        textAlign: "center",
        padding: 20,


    },

    inspection: {

        width: 370,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        marginTop: 50,
        overflow: 'hidden',

    },
    pic_men: {
        justifyContent: "space-evenly",
        flexDirection: "row",
        marginTop: 10,
        width: screenWidth,
        backgroundColor: 'red',

    },

    text_pics: {
        textAlign: 'center',
        color: '#fff',
        padding: 8,

    },

    footer: {

        justifyContent: 'space-evenly',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000'



    },

    footer_text: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text2: {

        backgroundColor: 'green',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

    footer_text3: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,


    },

    footer_text4: {

        backgroundColor: '#000',
        textAlign: 'center',
        color: '#fff',
        padding: 20,

    },

});