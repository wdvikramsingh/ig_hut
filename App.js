import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import Login from './app/component/login'
import Otp from './app/component/otp'
import Inspections from './app/component/inspections'
import Dueinspections from './app/component/dueinspections'
import Start from './app/component/start'
import Startsecond from './app/component/startsecond'
import Startthird from './app/component/startthird'
import Complaints from './app/component/complaints'
import Electrician from './app/component/electrician'
import Issuewithac from './app/component/issuewithac'
import Feedbackcomplaints from './app/component/feedbackcomplaints'
import Report from './app/component/report'
import Plumber from './app/component/plumber'
import Carpenter from './app/component/carpenter'
import Feedbackrating from './app/component/feedbackrating'
import Feedbackmess from './app/component/feedbackmess'


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


export default function App() {
    return (

        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Complaints">
                <Drawer.Screen name="Login" component={Login} />
                <Drawer.Screen name="Otp" component={Otp} />
                <Drawer.Screen name="Inspections" component={Inspections} />
                <Drawer.Screen name="Dueinspections" component={Dueinspections} />
                <Drawer.Screen name="Start" component={Start} />
                <Drawer.Screen name="Startsecond" component={Startsecond} />
                <Drawer.Screen name="Startthird" component={Startthird} />
                <Drawer.Screen name="Complaints" component={Complaints} />
                <Drawer.Screen name="Electrician" component={Electrician} />
                <Drawer.Screen name="Issuewithac" component={Issuewithac} />
                <Drawer.Screen name="Feedbackcomplaints" component={Feedbackcomplaints} />
                <Drawer.Screen name="Report" component={Report} />
                <Drawer.Screen name="Plumber" component={Plumber} />
                <Drawer.Screen name="Carpenter" component={Carpenter} />
                <Drawer.Screen name="Feedbackrating" component={Feedbackrating} />
                <Drawer.Screen name="Feedbackmess" component={Feedbackmess} />
            </Drawer.Navigator>
        </NavigationContainer>


    );
}
